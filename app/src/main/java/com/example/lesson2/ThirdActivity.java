package com.example.lesson2;

import android.app.Activity;
import android.os.Bundle;

public class ThirdActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.third_activity);
    }
}
